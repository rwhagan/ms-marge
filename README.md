# MS-MARGE
# Mpi-Shh-Mascot Report GEnerator


This script is intended for generating summary reports from .csv files
esported from Mascot. When exporting from Mascot, it is necessary
to deselect the box marked "Group Protein Families". No other
special options are necessary. The script works by using the file 
ms-marge.Rmd to generate an html report. The output of the html report
as well as a .csv of all peptides with unique queries, the E-value cutoff
you supply, and the minimum number of peptides you specify will all be 
output into the same folder that your input .csv is in. Additionally, a .fasta 
file with every peptide sequence meeting these criteria will be output. 
No original files will be overwritten.

### Prerequisites

MS-MARGE is dependent on the following software being installed on your system

1. [The R Project for Statistical Computing](https://cloud.r-project.org)

* R packages
	* Rmarkdown
	* tidyverse
	* knitr

To install R packages, start R from your command line via:

```
R
```

And then install all the R packages via the following:

```
install.packages(c("rmarkdown", "tidyverse", "knitr"))
```

2. [pandoc](https://pandoc.org)

Install pandoc for your operating system according to the instructions [here](https://pandoc.org/installing.html)


### Installing

Clone the repository and make the script executable using the following commands in the shell of your choice

```
git clone https://rwhagan@bitbucket.org/rwhagan/ms-marge.git

cd ms-marge

chmod +x MS-MARGE.R
```

## Getting help

Get help with running the script with the following command

```
./MS-MARGE.R --help
```

## Built With

* [The R Project for Statistical Computing](https://cloud.r-project.org) - Programming framework
* [tidyverse](http://tidyverse.org) - R packages for data science
* [Rmarkdown](http://rmarkdown.rstudio.com)
* [pandoc](http://pandoc.org)
* [knitr](https://yihui.name/knitr/) - Elegant, flexible, and fast dynamic report generation with R

## Author

* **Richard Hagan** [rwhagan on bitbucket.org](http://bitbucket.org/rwhagan)


